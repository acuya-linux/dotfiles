
alias ps3='sudo xboxdrv --detach-kernel-driver --led 2'
alias ls='ls --group-directories-first --color=auto'
alias vi='vim -u ~/.vimrc.custom'
alias vim='vim -u ~/.vimrc.custom'
alias reload-bash='. ~/.bashrc'
alias source-bash='source ~/.bashrc'

alias new-paper='cp -r ~/Templates/latex/ieee-latex .; cd ieee-latex; git init; vim main.tex'
alias new-empty='cp -r ~/Templates/latex/empty .; cd empty; git init; vim main.tex'

alias recompile='make; sudo make clean install'

alias amigo='env BUILD_TYPE=development SSLKEYPASSWORD=IAmADeveloperILoveAmigoCloud# ./docker_helper.sh'

alias amigo-parcel='go-amigo-frontend; yarn dev --max-old-space-size=5000'

alias amigo-up='echo "killing containers"; docker rm --force $(docker ps -a -q); echo "increasing docker client timeout";DOCKER_CLIENT_TIMEOUT=120; echo "increasing compose http timeout"; COMPOSE_HTTP_TIMEOUT=120; cd ~/dev/amigocloud/amigoserver/; echo "deleting .pyc files"; shopt -s globstar; rm -rf -- **/*.pyc; echo "launching containers"; amigo up -d && docker kill master_frontend_1 master_celery_backup_1  master_celery_system_1 master_celerybeat_1 master_flower_1 master_logrotator_1; go-amigo-frontend; nvm use 11.11; amigo-parcel'

alias amigo-restart='docker-killall && amigo-up'
alias go-amigo-frontend='cd ~/dev/amigocloud/amigoserver/django/amigoserver/frontend/app/dashboard_app'
alias go-amigo-test='cd /home/turo/dev/amigocloud/amigoserver/django/amigoserver/frontend/app/dashboard_app/puppeteer-tests'
alias amigo-add='git add -A; yarn lint-staged'
alias amigo-test='cd /home/turo/dev/amigocloud/amigoserver/django/amigoserver/frontend/app/dashboard_app; yarn test --projects puppeteer-tests -t'
alias tunnel-condor='ssh -A -L 8080:10.1.1.241:80 condor -N'

alias grf='cd /home/turo/dev/quality/ranking/frontend'
alias cf='cd ~/dev/quality/cursos-frontend'
alias cb='cd ~/dev/quality/cursos-backend'
alias cb2='cd ~/dev/quality/cursos-backend-v2'
alias ci='cd ~/dev/quality/calidad-integral'

alias dlm='youtube-dl -x --audio-format mp3 '
alias manage-new-songs='python3 ~/dev/dotfiles/bin/manage-new-songs.py'
alias music='vlc --random ~/Music/all'

alias pyvenv='source venv/bin/activate'
alias source-conda='source ~/dev/software/miniconda3/bin/activate base'

alias delete-pyc='shopt -s globstar; sudo rm -r -- **/*.pyc'

alias untar='tar -zxvf'

alias new-ocsa='cp -r ~/Templates/code/new-ocsa .; python3 ~/dev/dotfiles/bin/new-ocsa.py'

alias docker-killall='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'
alias dps='python3 ~/dev/dotfiles/bin/docker-ps.py'

alias koans='cd ~/dev/tutos/ruby_koans/; ruby path_to_enlightenment.rb'

alias logout='i3-msg exit'

alias aliases='less ~/dev/dotfiles/bash_aliases'

alias sshare-on='xrandr --output eDP-1 --off'
alias sshare-off='xrandr --output eDP-1-1 --auto; xrandr --output eDP-1-1 --primary --left-of HDMI-0'

alias save-dotfiles='cd ~/dev/dotfiles; git add -A; git commit -m 'change'; git push origin master'

alias vultr='ssh root@45.63.106.213'
alias deploy-cursos='ssh root@45.63.106.213 cd /root/cursos-backend/ && /root/cursos-backend/deploy.sh'

alias ezy='cd ~/dev/ezy-react'
alias kl='cd ~/dev/kuya/landing-kd'
alias act='cd ~/dev/inqube/app-actividades-react-native'
alias bact='cd ~/dev/inqube/backend-actividades'
alias qcl='cd ~/dev/quality/cluster/config'
