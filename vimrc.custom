" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" CUSTOM CONFIGURATION
set directory^=$HOME/.vim/tmp//
set number "Show line number
set ttimeoutlen=30 "No delay when changing modes

" Automatically install vim-plug if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

Plug 'airblade/vim-gitgutter' "Git flags next to line numbers
Plug 'editorconfig/editorconfig-vim' "EditorConfig for Vim
Plug 'itchyny/lightline.vim' "Cool statusline
Plug 'junegunn/fzf' "Find files by name
Plug 'junegunn/fzf.vim' "Dependency of fzf
Plug 'scrooloose/nerdtree' "Files and directories panel
Plug 'tpope/vim-surround' "Surround text with brackets, e.g: [],{},()
Plug 'tpope/vim-eunuch' "Unix like commands inside Vim, e.g: rename, chmod
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'https://github.com/qpkorr/vim-renamer' "Rename batch of files
"Plug 'prettier/vim-prettier', {
"  \ 'do': 'yarn install',
"  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
Plug 'tpope/vim-fugitive' "Git wrapper
Plug 'posva/vim-vue' "Syntax highlighting for Vue
Plug 'bfrg/vim-cpp-modern' "Syntax for c/c++
Plug 'leafgarland/typescript-vim' "Syntax for ts
Plug 'maxmellon/vim-jsx-pretty'
Plug 'peitalin/vim-jsx-typescript'
call plug#end()

filetype plugin on
syntax on
set nocompatible

" Vim preview config
let g:livepreview_previewer = 'zathura'

"Lightline config
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }
set laststatus=2
set noshowmode

"Prettier config
let g:prettier#autoformat_require_pragma = 0
let g:prettier#autoformat_config_present = 1

" So Gdiff shows vertical tabs
set diffopt+=vertical

" For amigocloud
set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
inoremap ,. <Esc>:let @t=@0<Enter>vF<lyA></<Esc>pA><Esc>hF>l:let @0=@t<Enter>i

" set filetypes as typescript.tsx
autocmd BufNewFile,BufRead *.tsx,*.jsx set filetype=typescript.tsx

"Remapping of functions
map fzf :Files<CR>
map <C-o> :NERDTreeToggle<CR>
map latex :LLPStartPreview

"Go to next <++>
inoremap ,, <Esc>/<++><Enter>"_c4l

"Formatting music files' names

"Delete all garbage text that youtube-dl puts at the end of file name
nnoremap <Space>b <Esc>$F.hvF-"_x

"Delete everything from cursor to .ext
nnoremap <Space>n <Esc>vf.h"_x

"If the format is <Autor> - <Song>
"rename it to <Song> - <Author> 
nnoremap <Space>m <Esc>0f/lvf-2hxv2l"_x$F.i - <Esc>pj 

"Go to next line without " - "
nnoremap .. <Esc>/^\(\( - \)\@!.\)*$<Enter>zz

"If the song has no name, go to next .mp3 and prepare to insert artist name
nnoremap <Space><Space> <Esc>$F.i - <Esc>li

"Latex config
inoremap ,h1 \section{}<Enter><++><Esc>k$F{li
inoremap ,h2 \subsection{}<Enter><++><Esc>k$F{li
inoremap ,h3 \subsubsection{}<Enter><++><Esc>k$F{li
inoremap ,b \textbf{}<Space><++><Esc>F{li
inoremap ,i \textit{}<Space><++><Esc>F{li
inoremap ,eq \[\]<Enter><++><Esc>k$F[li

inoremap ,fig \begin{figure}[h]<Enter><Tab>\centering<Enter><Tab>\includegraphics[]{<++>}<Enter><Tab>\caption{<++>}<Enter><Tab>\label{<++>}<Enter>\end{figure}<Enter><Enter><++><Esc>5k$F[li

inoremap ,lst \begin{lstlisting}[language=, title={<++>}]<Enter><++><Enter>\end{lstlisting}<Enter><Enter><++><Esc>4k$F,i

inoremap ,ul \begin{itemize}<Enter>\item<Space><Enter>\end{itemize}<Enter><++><Esc>2k$i

inoremap ,ol \begin{enumerate}<Enter>\item<Space><Enter>\end{enumerate}<Enter><++><Esc>2k$i

inoremap ,cl console.log()<Esc>i
inoremap ,ll <Esc>yyP$i'<Esc>F(li'<Esc>j$

set backupcopy=yes
