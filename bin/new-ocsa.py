import os
import pathlib

cwd = os.path.join(os.getcwd(), 'new-ocsa')

lab_n = input('Lab number: ')
struct_name = input('Structure name: ')

pathlib.Path(cwd + '/include/utec/' + struct_name).mkdir(parents=True, exist_ok=True)

pathlib.Path(cwd + '/src/' + struct_name).mkdir(parents=True, exist_ok=True)

pathlib.Path(cwd + '/tests/' + struct_name).mkdir(parents=True, exist_ok=True)

pathlib.Path(cwd + '/include/utec/' + struct_name + '/' + struct_name + '.hpp').touch()

pathlib.Path(cwd + '/src/' + struct_name + '/' + struct_name + '.cpp').touch()

pathlib.Path(cwd + '/tests/' + struct_name + '/' + struct_name + '.cpp').touch()

with open(cwd + '/CMakeLists.txt') as file:
    lines = file.readlines()

lines[115] = (' ')*12 + '${CMAKE_CURRENT_SOURCE_DIR}/include/utec/' + struct_name + '/'

lines[118] = (' ')*12 + 'src/' + struct_name + '/' + struct_name + '.cpp'

lines[121] = (' ')*12 + 'tests/' + struct_name + '/' + struct_name + '.cpp'

with open(cwd + '/CMakeLists.txt', 'w') as file:
    file.writelines(lines)
    
os.rename(cwd, 'lab' + lab_n + '-' + struct_name)
