from os import listdir, symlink, remove, makedirs, rename
from os.path import isfile, join, isdir
from shutil import copy2
from sys import exit as end
from difflib import SequenceMatcher

# For testing
#base_dir = '/home/turo/music-save/Music/'

base_dir = '/home/turo/Music/'
new_songs_dir = base_dir + 'new/'
all_songs_dir = base_dir + 'all/'
artists_dir = base_dir + 'artists/'

new_songs = [song for song in listdir(new_songs_dir) if isfile(join(new_songs_dir, song))]

existing_artists = [artist for artist in listdir(artists_dir) if isdir(join(artists_dir, artist))]

def check_similar_artist(artist_name):
    similarity = 0.0
    for existing_artist_name in existing_artists:
        similarity = SequenceMatcher(None, artist_name, existing_artist_name).ratio()
        if similarity >= 0.8 and similarity is not 1.0:
            return (True, existing_artist_name)

    return (False,'')

if len(new_songs) == 0:
    print('No new songs')
    end()

for song in new_songs:
    og_name = song
    print('Processing: ', og_name)

    # Remove extension from filename
    song, extension = song.rsplit('.',1)

    song_name, artist_name = song.split(' - ')
    
    similar_artist = check_similar_artist(artist_name)

    # If a similar artist name already exists
    if similar_artist[0]:
        print('\n========')
        print('It appears that there\'s already an artist called "',similar_artist[1],'in your library. Do you want to keep the existing name?')
        print('Option 1: Yes, keep the existing name (',similar_artist[1], ')')
        print('Option 2: No. The artist"', artist_name, '"is someone else')
        print('========\n')

        option_selected = input('Option selected: ')

        # Change file name to match existing artist name
        if option_selected == '1':
            artist_name = similar_artist[1]
            new_og_name = song_name + ' - ' + artist_name + '.' + extension
            rename(join(new_songs_dir, og_name), new_og_name)
            og_name = new_og_name
        else:
            # If the Artist's directory doesn't exist, create it
            if not isdir(join(artists_dir, artist_name)):
                makedirs(join(artists_dir, artist_name))
                print('Directory for', artist_name, 'doesn\'t exist. A new directory will be created for it.')
    
    # Copy new song from new/ to all/
    copy2(join(new_songs_dir, og_name), all_songs_dir)

    # Create symlink of song from all/ to artists/{artist}
    try:
        symlink(join(all_songs_dir, og_name), join(artists_dir, artist_name + '/', og_name))
    except:
        print('Song already exists, omitting symlink')

    # Remove songs from new/
    remove(join(new_songs_dir, og_name))

print('Finished processing', str(len(new_songs)), 'songs')
