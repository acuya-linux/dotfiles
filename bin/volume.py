# Increase, decrease or mute volume of active Pulseaudio sink

import subprocess
import sys

action = sys.argv[1]
amount = sys.argv[2]

list_sinks_cmd = 'pactl list sinks'

pactl_output = subprocess.run(list_sinks_cmd.split(), stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

running_sink_index = 0

for i in range(0, len(pactl_output)):
    if pactl_output[i] == "\tState: RUNNING":
        running_sink_index = i - 1
        i = len(pactl_output) + 1

running_sink_string = pactl_output[running_sink_index]
running_sink = running_sink_string[-1:]

command = ''

if action == 'change':
    command = 'pactl set-sink-volume '
else:
    command = 'pactl set-sink-mute '

volume_cmd = command + running_sink + ' ' + amount
subprocess.run(volume_cmd.split(), stdout=subprocess.PIPE)
