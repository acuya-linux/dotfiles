currentdir=$(pwd)
currentdir=$currentdir"/../"

ln -s $currentdir"bash_aliases" ~/.bash_aliases
ln -s $currentdir"vimrc.custom" ~/.vimrc.custom
ln -s $currentdir"zathurarc" ~/.config/zathura/zathurarc
ln -s $currentdir"bashrc" ~/.bashrc
ln -s $currentdir"i3_config" ~/.config/i3/config
ln -s $currentdir"polybar_config" ~/.config/polybar/config
ln -s $currentdir"bin/launch_polybar.sh" ~/.config/polybar/launch_polybar.sh
ln -s $currentdir"xinitrc" ~/.xinitrc
ln -s $currentdir"Xdefaults" ~/.Xdefaults
ln -s $currentdir"urxvt/" ~/.urxvt
