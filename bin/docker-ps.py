import os
import re
import tempfile as tmp
import sys

def terminal_height():
    import fcntl, termios, struct
    h, w, hp, wp = struct.unpack('HHHH',
        fcntl.ioctl(0, termios.TIOCGWINSZ,
        struct.pack('HHHH', 0, 0, 0, 0)))
    return h

ps = os.popen('sudo docker ps').read().splitlines()
headers = re.split(r'  +', ps[0])

# Remove headers row
ps.pop(0)

rows = len(ps)

if (rows == 0):
    print('No containers running')
    sys.exit()

# Separate row in columns
for r in range(0, rows):
    ps[r] = re.split(r'  +', ps[r])

# Add "None" if port column is empty
for row in ps:
    if len(row) == 6:
        row.insert(5, 'None')

cols = len(headers)

output = '==========\n'

# Build the output string
for r in range (0, rows):
    for c in range (0, cols):
        output += headers[c] + ':' + ps[r][c] + "\n"
    output += '==========\n'

if ((cols + 2) * rows) <= terminal_height():
    print(output)
    sys.exit()

# Create tmp file with the output
# so it can be read by less
f, filename = tmp.mkstemp()

try:
    os.write(f, str.encode(output))
    os.close(f)
    os.system('less ' + filename)
finally:
    os.remove(filename)
